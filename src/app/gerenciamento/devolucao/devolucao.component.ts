import { Component, OnInit } from '@angular/core';
import { RequestDevolucao, ResponseDevolucao } from "../gerenciamento.model";
import { GerenciamentoService } from "../gerenciamento.service";

@Component({
  selector: 'app-devolucao',
  templateUrl: './devolucao.component.html',
  styleUrls: ['./devolucao.component.css']
})
export class DevolucaoComponent implements OnInit {
  
  request: RequestDevolucao = {
    titulo: "",
    disponivel: null
  }

  response: ResponseDevolucao

  constructor(private gerenciamentoService: GerenciamentoService) { }

  ngOnInit() {
  }

  save() {
    this.gerenciamentoService.realizarDevolucao(this.request).subscribe(res => {
      this.response = res;
    })
  }
}
