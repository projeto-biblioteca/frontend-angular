import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { RequestCadastro, ResponseCadastro, ResponseBusca } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private url = "http://localhost:8080/api/v1/clientes/"

  constructor(private http: HttpClient) { }

  createUser(request: RequestCadastro): Observable<ResponseCadastro>{
    return this.http.post<ResponseCadastro>(this.url, request)
  }

  getUser(cpf: number): Observable<ResponseBusca>{
    const _url = `${this.url}/${cpf}`
    return this.http.get<ResponseBusca>(_url)
  }
}
