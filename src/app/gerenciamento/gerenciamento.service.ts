import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { RequestEmprestimo, ResponseEmprestimo } from "./gerenciamento.model";

@Injectable({
  providedIn: 'root'
})
export class GerenciamentoService {

  private url = "http://localhost:8082/api/v1/gerenciamento"

  constructor(private http: HttpClient) { }

  realizarEmprestimo(request: RequestEmprestimo): Observable<ResponseEmprestimo>{
    const _urlEmprestimo = `${this.url}/emprestimo`
    return this.http.post<ResponseEmprestimo>(_urlEmprestimo, request)
  }

  realizarDevolucao(request: RequestEmprestimo): Observable<ResponseEmprestimo>{
    const _urlDevolucao = `${this.url}/devolucao`
    return this.http.post<ResponseEmprestimo>(_urlDevolucao, request)
  }

}
