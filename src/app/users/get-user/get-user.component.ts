import { Component, OnInit } from '@angular/core';
import { UsersService } from "../users.service";
import { ResponseBusca } from "../user.model";

@Component({
  selector: 'app-get-user',
  templateUrl: './get-user.component.html',
  styleUrls: ['./get-user.component.css']
})
export class GetUserComponent implements OnInit {

  cpf: number;
  response: ResponseBusca;

  constructor(private userService: UsersService) { }

  ngOnInit() {
  }

  search() {
    this.userService.getUser(this.cpf).subscribe(res => {
      this.response = res;
    })
  }
}
