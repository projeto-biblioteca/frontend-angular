export interface RequestCadastro {
    nome: string;
    cpf: number;
    telefone: string;
    dataNascimento: number[];
}

export interface Response {
    warnings: any[];
    erros: any[];
    infos: any[];
}

export interface ResponseCadastro {
    response: Response;
    id: string;
    nome: string;
    cpf: number;
    telefone: string;
    dataNascimento: string;
}

/**get user por cpf */
export interface ResponseBuscaInfo {
    warnings: any[];
    erros: any[];
    infos: any[];
}

export interface ResponseBusca {
    response: ResponseBuscaInfo;
    id: string;
    nome: string;
    cpf: number;
    telefone: string;
    dataNascimento: string;
}