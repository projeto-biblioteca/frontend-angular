export interface RequestCadastro {
    titulo: string;
    autor: string;
    editora: string;
    dataPublicacao: number[];
    numeroPaginas: number;
}

export interface Response {
    warnings: any[];
    erros: any[];
    infos: any[];
}

export interface ResponseCadastro {
    response: Response;
    titulo: string;
    autor: string;
    editora: string;
    dataPublicacao: string;
    numeroPaginas: number;
    id: string;
    disponivel?: any;
}

/**get livro por titulo */
export interface ResponseBuscaInfo {
    warnings: any[];
    erros: any[];
    infos: any[];
}

export interface ResponseBusca {
    response: ResponseBuscaInfo;
    titulo: string;
    autor: string;
    editora: string;
    dataPublicacao: string;
    numeroPaginas: number;
    id?: any;
    disponivel?: number;
}