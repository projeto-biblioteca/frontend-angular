import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { CreateUserComponent } from './users/create-user/create-user.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from "@angular/forms";
import { LivrosComponent } from './livros/livros.component';
import { CreateLivroComponent } from './livros/create-livro/create-livro.component';
import { GetLivroComponent } from './livros/get-livro/get-livro.component';
import { GetUserComponent } from './users/get-user/get-user.component';
import { GerenciamentoComponent } from './gerenciamento/gerenciamento.component';
import { EmprestimoComponent } from './gerenciamento/emprestimo/emprestimo.component';
import { DevolucaoComponent } from './gerenciamento/devolucao/devolucao.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    CreateUserComponent,
    LivrosComponent,
    CreateLivroComponent,
    GetLivroComponent,
    GetUserComponent,
    GerenciamentoComponent,
    EmprestimoComponent,
    DevolucaoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
