import { Component, OnInit } from '@angular/core';
import { LivrosService } from '../livros.service';
import { ResponseBusca } from '../livro.model';

@Component({
  selector: 'app-get-livro',
  templateUrl: './get-livro.component.html',
  styleUrls: ['./get-livro.component.css']
})
export class GetLivroComponent implements OnInit {

  titulo: string;
  response: ResponseBusca;

  constructor(private livroService: LivrosService) { }

  ngOnInit() {
  }

  search() {
    this.livroService.getLivro(this.titulo).subscribe(res => {
      this.response = res;
    })
  }
}
