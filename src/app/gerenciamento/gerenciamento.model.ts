/**post Emprestimo */
export interface ResponseEmprestimoInfo {
    warnings: any[];
    erros: any[];
    infos: any[];
}

export interface ResponseEmprestimo {
    response: ResponseEmprestimoInfo;
    titulo: string;
    autor: string;
    editora: string;
    dataPublicacao: string;
    numeroPaginas: number;
    id: string;
    disponivel?: number;
}

export interface RequestEmprestimo {
    titulo: string;
    disponivel: number;
}

/**post Devolucao */
export interface ResponseDevolucaoInfo {
    warnings: any[];
    erros: any[];
    infos: any[];
}

export interface ResponseDevolucao {
    response: ResponseDevolucaoInfo;
    titulo: string;
    autor: string;
    editora: string;
    dataPublicacao: string;
    numeroPaginas: number;
    id: string;
    disponivel?: number;
}

export interface RequestDevolucao {
    titulo: string;
    disponivel: number;
}