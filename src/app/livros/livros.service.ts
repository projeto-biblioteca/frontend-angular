import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { RequestCadastro, ResponseCadastro, ResponseBusca } from './livro.model';

@Injectable({
  providedIn: 'root'
})
export class LivrosService {

  private url = "http://localhost:8081/api/v1/livros"

  constructor(private http: HttpClient) { }

  createLivro(request: RequestCadastro): Observable<ResponseCadastro>{
    return this.http.post<ResponseCadastro>(this.url, request)
  }

  getLivro(titulo: string): Observable<ResponseBusca>{
    const _url = `${this.url}/${titulo}`
    return this.http.get<ResponseBusca>(_url)
  }
}
