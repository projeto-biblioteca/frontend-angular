import { Component, OnInit } from '@angular/core';
import { RequestEmprestimo, ResponseEmprestimo } from "../gerenciamento.model";
import { GerenciamentoService } from "../gerenciamento.service";

@Component({
  selector: 'app-emprestimo',
  templateUrl: './emprestimo.component.html',
  styleUrls: ['./emprestimo.component.css']
})
export class EmprestimoComponent implements OnInit {

  request: RequestEmprestimo = {
    titulo: "",
    disponivel: null
  }

  response: ResponseEmprestimo

  constructor(private gerenciamentoService: GerenciamentoService) { }

  ngOnInit() {
  }

  save() {
    this.gerenciamentoService.realizarEmprestimo(this.request).subscribe(res => {
      this.response = res;
    })
  }

}
