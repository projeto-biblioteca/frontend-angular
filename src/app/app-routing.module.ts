import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateUserComponent } from './users/create-user/create-user.component';
import { CreateLivroComponent } from './livros/create-livro/create-livro.component';
import { GetLivroComponent } from './livros/get-livro/get-livro.component';
import { GetUserComponent } from "./users/get-user/get-user.component";
import { DevolucaoComponent } from "./gerenciamento/devolucao/devolucao.component";
import { EmprestimoComponent } from "./gerenciamento/emprestimo/emprestimo.component";


const routes: Routes = [
  {path: 'users/create', component: CreateUserComponent},
  {path: 'users/get', component: GetUserComponent},
  {path: 'livros/create', component: CreateLivroComponent},
  {path: 'livros/get', component: GetLivroComponent},
  {path: 'gerenciamento/emprestimo', component: EmprestimoComponent},
  {path: 'gerenciamento/devolucao', component: DevolucaoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
