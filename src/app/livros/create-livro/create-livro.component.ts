import { Component, OnInit } from '@angular/core';
import { LivrosService } from '../livros.service';
import { RequestCadastro, ResponseCadastro } from '../livro.model';


@Component({
  selector: 'app-create-livro',
  templateUrl: './create-livro.component.html',
  styleUrls: ['./create-livro.component.css']
})
export class CreateLivroComponent implements OnInit {

  request: RequestCadastro = {
    autor: "",
    dataPublicacao: [],
    editora: "",
    numeroPaginas: null,
    titulo: ""
  }

  response: ResponseCadastro

  constructor(private livroService: LivrosService) { }

  ngOnInit() {
  }

  save() {
    this.livroService.createLivro(this.request).subscribe(res => {
      this.response = res;
    })
  }
}
